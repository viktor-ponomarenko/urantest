package tk.davinctor.model

/**
 * @author Victor Ponomarenko
 */
data class Exhibit(val title: String, val images: List<String>)
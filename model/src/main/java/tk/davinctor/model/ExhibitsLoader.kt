package tk.davinctor.model

/**
 * @author Victor Ponomarenko
 */
interface ExhibitsLoader {

    fun getExhibitList(): List<Exhibit>

}
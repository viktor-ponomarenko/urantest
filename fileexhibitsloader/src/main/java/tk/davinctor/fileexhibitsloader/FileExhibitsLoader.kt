package tk.davinctor.fileexhibitsloader

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import tk.davinctor.model.Exhibit
import tk.davinctor.model.ExhibitsLoader
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import com.google.gson.reflect.TypeToken
import java.util.*


/**
 * @author Victor Ponomarenko
 */
class FileExhibitsLoader(context: Context) : ExhibitsLoader {

    private val appContext: Context = context.applicationContext
    private val fileName: String = "data.json"
    private val logTag = "FileExhibitsLoader"

    override fun getExhibitList(): List<Exhibit> {
        var reader: Reader? = null
        try {
            val stringBuilder = StringBuilder()
            reader = BufferedReader(InputStreamReader(appContext.assets.open(fileName), "UTF-8"))
            reader.lines.forEach {
                stringBuilder.append(it)
            }
            val listType = object : TypeToken<Wrapper>() {}.type
            val wrapper: Wrapper = Gson().fromJson(stringBuilder.toString(), listType)
            return wrapper.list
        } catch (e: Exception) {
            Log.e(logTag, e.message, e)
            throw FailedToLoadExhibitsException(e)
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException) {
                    Log.e(logTag, e.message, e)
                }
            }
        }
    }

}

internal data class Wrapper(val list: List<Exhibit>)

internal val BufferedReader.lines: Iterator<String>
    get() = object : Iterator<String> {

        var line = this@lines.readLine()

        override fun next(): String {
            if (line == null)
                throw NoSuchElementException()

            val result = line!!
            line = this@lines.readLine()
            return result
        }

        override fun hasNext() = line != null

    }
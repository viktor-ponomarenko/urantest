package tk.davinctor.fileexhibitsloader

/**
 * @author Victor Ponomarenko
 */
class FailedToLoadExhibitsException(cause: Exception) : Exception(cause)
package tk.davinctor.urantest

import tk.davinctor.model.Exhibit

/**
 * @author Victor Ponomarenko
 */
data class LoadResult(val exhibits: List<Exhibit>, val error: Throwable?)
package tk.davinctor.urantest

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import tk.davinctor.model.ExhibitsLoader
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * @author Victor Ponomarenko
 */
class ExhibitsRepositoryImpl(private val exhibitsLoader: ExhibitsLoader) : ExhibitsRepository {

    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    override fun getExhibits(): LiveData<LoadResult> {
        val exhibitsLiveData = MutableLiveData<LoadResult>()
        executorService.submit({
            try {
                exhibitsLiveData.postValue(LoadResult(exhibitsLoader.getExhibitList(), null))
            } catch (e: Exception) {
                exhibitsLiveData.postValue(LoadResult(Collections.emptyList(), e))
            }
        })
        return exhibitsLiveData
    }

}
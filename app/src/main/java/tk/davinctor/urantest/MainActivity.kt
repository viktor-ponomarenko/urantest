package tk.davinctor.urantest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import tk.davinctor.fileexhibitsloader.FileExhibitsLoader
import tk.davinctor.model.Exhibit

class MainActivity : AppCompatActivity() {

    private val exhibits: ArrayList<Exhibit> = ArrayList()
    private val exhibitsAdapter = ExhibitsAdapter(exhibits)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val layoutManager = GridLayoutManager(this, 2)
        exhibits_rv.layoutManager = layoutManager
        exhibits_rv.adapter = exhibitsAdapter
        withViewModel({ ExhibitsViewModel(provideExhibitsRepository()) }) {
            observe(exhibitsLoadResult, ::updateUi)
        }
    }

    private fun updateUi(loadResult: LoadResult?) {
        if (loadResult!!.error != null) {
            exhibits_rv.visibility = View.GONE
            error_tv.text = loadResult.error!!.message
            error_tv.visibility = View.VISIBLE
        } else {
            error_tv.visibility = View.GONE
            exhibits_rv.visibility = View.VISIBLE
            exhibits.addAll(loadResult.exhibits)
            exhibitsAdapter.notifyItemRangeInserted(0, exhibits.size)
        }
    }

    private fun provideExhibitsRepository() = ExhibitsRepositoryImpl(FileExhibitsLoader(applicationContext))

}

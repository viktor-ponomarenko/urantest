package tk.davinctor.urantest

import android.content.Context
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * @author Victor Ponomarenko
 */
class ExhibitPhotosAdapter(private val context: Context, private val photos: List<String>) : PagerAdapter() {

    private val layoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int = photos.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val pager = container as ViewPager
        val view: ImageView = layoutInflater.inflate(R.layout.item_exhibit_photo, container, false) as ImageView
        Glide.with(context)
                .load(photos[position])
                .into(view)
        pager.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        (container as ViewPager).removeView(view as View)
    }
}
package tk.davinctor.urantest

import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import tk.davinctor.model.Exhibit

/**
 * @author Victor Ponomarenko
 */
class ExhibitsAdapter(private val exhibits: List<Exhibit>) : RecyclerView.Adapter<ExhibitsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_exhibit, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = exhibits.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(exhibits[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val nameTP = itemView.findViewById<TextView>(R.id.exhibit_name_tv)
        private val photosVP = itemView.findViewById<ViewPager>(R.id.exhibit_photos_vp)

        private val photos = ArrayList<String>()
        private val photosVPAdapter = ExhibitPhotosAdapter(itemView.context, photos)

        init {
            photosVP.adapter = photosVPAdapter
        }

        fun bind(exhibit: Exhibit) {
            nameTP.text = exhibit.title
            photos.addAll(exhibit.images)
            photosVPAdapter.notifyDataSetChanged()
        }

    }

}
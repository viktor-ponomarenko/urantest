package tk.davinctor.urantest

import android.arch.lifecycle.LiveData

/**
 * @author Victor Ponomarenko
 */
interface ExhibitsRepository {

    fun getExhibits() : LiveData<LoadResult>

}
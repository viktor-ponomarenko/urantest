package tk.davinctor.urantest

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel

/**
 * @author Victor Ponomarenko
 */
class ExhibitsViewModel(exhibitsRepository: ExhibitsRepository) : ViewModel() {

    val exhibitsLoadResult: LiveData<LoadResult> = exhibitsRepository.getExhibits()

}